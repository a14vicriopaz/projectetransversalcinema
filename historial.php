<?php

$correuErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $correu = test_input($_POST["correu"]);
    if (!filter_var($correu, FILTER_VALIDATE_EMAIL)) {
      $correuErr = "Invalid email format"; 
    }

}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

?>

<!DOCTYPE html>
<html lang="es">
<head>
<title>HISTORIAL</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/colors.css">
<link rel="stylesheet" type="text/css" href="css/estructura.css">
<link rel="stylesheet" type="text/css" href="css/estructuraPP.css?">
<link rel="stylesheet" type="text/css" href="css/fonts.css">
    
</head>
<body class="borange">
    <div id="morado">
        <div id="cos">
            <div id="titol">
                <h1>Historial</h1>
            </div>
            <div class="main_box_botons_historial">
                <div class="box_botons_historial">
                    <a class="btn bblue black" href="index.php">Enrere</a>
                    <a class="btn bblue black" href="admin/admin.html">Admin</a>
                </div>
            </div>
    <div class="formulario">
        <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">
        <br>
        <br>
        <b>Introdueix el teu correu:</b>
        <br>
        <br>
        Correu: <input type="text" name="correu" required>
        <span class="error"> <?php echo $correuErr;?></span>
    </div>
    <div class="margin">
        <input class="btn" type="submit" name="cercar" value="Cercar">    
    </div>
    
    </form>
    
    <?php

    $correu = $_POST['correu'];

    require('database.php');

    if(isset($_POST['cercar'])){   

        $hoy=date("Y-m-d");
        $hora=date("H:i:s");

        $consulta = "SELECT e.precio, e.butaca, p.nombre, s.fecha, s.hora FROM entrada e 
        INNER JOIN sesiones s ON e.id_sesion = s.id 
        INNER JOIN pelicula p ON s.id_pelicula = p.id 
        WHERE (fecha > '$hoy' AND correo = '$correu') OR (correo = '$correu' AND (fecha = '$hoy' AND hora > '$hora'))";
        
        $resultado = mysqli_query($conexion, $consulta) or die ("Problemes en la consulta!");

          if(!mysqli_num_rows($resultado)){ 

            echo "<div class='p_historial'>";
            echo "<div class='p_margin'>";
            echo "<h2>Sessió pendent</h2>";

            echo "<p>No tens pel·lícules pendents!</p>";
            echo "</div>";
            echo "</div>";
        }else{

                echo "<div class='p_historial'>";
                echo "<div class='p_margin'>";
                echo "<h2>Sessió pendent</h2>";

                while($columna1 = mysqli_fetch_array($resultado)){


                    echo "<b>Nom peli:</b> ".$columna1['nombre']."<br>";
                    echo "<b>Nº Butaques:</b> ".$columna1['butaca']."<br>"; 
                    echo "<b>Dia de la sessió:</b> ".$columna1['fecha']."<br>";
                    echo "<b>Hora de la sessió:</b> ".$columna1['hora']."<br>";
                    echo "<b>Preu final:</b> ".$columna1['precio']."<br><br>";
                    
                }

                echo "</div>";
                echo "</div>";
        }

        

        $consulta2 = "SELECT e.precio, e.butaca, p.nombre, s.fecha, s.hora FROM entrada e 
        INNER JOIN sesiones s ON e.id_sesion = s.id 
        INNER JOIN pelicula p ON s.id_pelicula = p.id 
        WHERE (fecha < '$hoy' AND correo = '$correu') 
        OR (correo = '$correu' AND (fecha = '$hoy' AND hora < '$hora'))";
        $resultado2 = mysqli_query($conexion, $consulta2) or die ("Problemes en la consulta!");
       
       if(!mysqli_num_rows($resultado2)){ 

                echo "<div class='p_historial'>";
                echo "<div class='p_margin'>";
                echo "<h2>Sessions passades</h2>";

                echo "<p>No tens pel·lícules passades!</p>";
                echo "</div>";
                echo "</div>";
        }else{

                echo "<div class='p_historial'>";
                echo "<div class='p_margin'>";
                echo "<h2>Sessions passades</h2>";
                
                while($columna2 = mysqli_fetch_array($resultado2)){
                
                echo "<b>Nom peli:</b> ".$columna2['nombre']."<br>";
                echo "<b>Nº Butaques:</b> ".$columna2['butaca']."<br>"; 
                echo "<b>Dia de la sessió:</b> ".$columna2['fecha']."<br>";
                echo "<b>Hora de la sessió:</b> ".$columna2['hora']."<br>";
                echo "<b>Preu final:</b> ".$columna2['precio']."<br><br>";
                }
                echo "</div>";
                echo "</div>";
                
        }

       
    }

    ?>
    <p id="demo">
    </p>

    <script type="text/javascript">

let nom=<?php echo json_encode($nom_peli);?>;
let butaques=<?php echo json_encode($butaques);?>;
let preu=<?php echo json_encode($preu);?>;

document.getElementById("demo").innerHTML = nom;



//console.log(horas);

</script>
    
            </div>
        </div>
    </body>
</html>