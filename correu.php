<?php
//cookies:

$dia=$_COOKIE['dia'];
$butaques=$_COOKIE['cbutaca'];
$preu_total=$_COOKIE['ptotal']; //preu total
$total_butaques=$_COOKIE['t_butaca'];

$correuErr = "";


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $correu = test_input($_POST["correu"]);
    if (!filter_var($correu, FILTER_VALIDATE_EMAIL)) {
      $correuErr = "Invalid email format"; 

    }

}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

?>

<!DOCTYPE html>
<html lang="es">
<head>
<title>COMPRA</title>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/colors.css">
<link rel="stylesheet" type="text/css" href="css/estructura.css">
<link rel="stylesheet" type="text/css" href="css/estructuraPP.css?">
<link rel="stylesheet" type="text/css" href="css/fonts.css">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript">

function validacion() {
valor = document.getElementById("correu").value;

if( !(/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(valor)) ) {

    swal({
                type: 'error',
                title: 'Oops...',
                text: 'El correu no es correcte',
                footer: '<a href>Why do I have this issue?</a>'
              })
  
    return false;
  
}else{

    return true;
}
}
</script>
    
</head>
<body class="borange">

<div id="cos">
            <div id="titol">
                <h1>COMPRA</h1>
            </div>
    <div class="formulario">
    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>" onsubmit="return validacion()">

    <b>Introdueix el teu correu:</b>
    <br>
    <br>
    <b>Correu:</b> <input type="text" name="correu" id="correu" required>
    <span class="error"> <?php echo $correuErr;?></span>
    <br>
    <br>
    <input class="btn" type="submit" name="comprueba" value="Comprobar Correo">    
    
    
    </form>
    </div>
    <?php

    $correu = $_POST['correu'];

    require('database.php');

    if(isset($_POST['comprueba'])){  
        
        //Comprueba si el usuario no existe

        $sql="SELECT correo FROM usuario WHERE correo = '$correu'";
        $result = mysqli_query($conexion, $sql) or die ("Problemes en la consulta!");
        
    
        if(!mysqli_num_rows($result)){
    
           //Si no existe q se registre!

           setcookie('correu', $correu, time()+3600);
           
           echo "<div class='correu'>";
           echo "<b id='n_registrar'>Necessitas registrarte!</b>";
           echo "</div>";
           echo "<br>";
           echo "<div  class='margin'>";
           echo '<a class="btn" href="form_registre.php">Formulari de registre.</a><br>';
           echo "</div>";
           echo "<br>";
        }else{
            echo "<div class='correu'>";
            echo "Ja estàs registrat!<br><br>";

            $hoy=date("Y-m-d");
            $hora=date("H:i:s");

            $consulta = "SELECT e.precio, e.butaca, p.nombre, s.fecha, s.hora FROM entrada e 
            INNER JOIN sesiones s ON e.id_sesion = s.id 
            INNER JOIN pelicula p ON s.id_pelicula = p.id 
            WHERE (fecha > '$hoy' AND correo = '$correu') OR (correo = '$correu' AND (fecha = '$hoy' AND hora > '$hora'))";
        
            $resultado = mysqli_query($conexion, $consulta) or die ("Problemes en la consulta!");
            

            if(!mysqli_num_rows($resultado)){

                //Si no tiene ninguna entrada pendiente...

             setcookie('correu', $correu, time()+3600);

             echo "</div>";
             echo"<br>";
             echo "<div  class='margin'>";
             echo '<a class="btn" href="form_compra.php">Formulari de compra.</a>';
             echo "</div>";
             echo"<br>";
            }else{

                echo "No pots comprar més entrades.<br>";

                echo "<p>Tens una pel.lícula pendent!</p>";

                while($columna = mysqli_fetch_array($resultado)){

                    
                    echo "<li><b>Nom peli: </b>".$columna['nombre']."</li>";
                    echo "<li><b>Nº Butaques: </b>".$columna['butaca']."</li>";
                    echo "<li><b>Dia de la sessió:</b> ".$columna['fecha']."</li>";
                    echo "<li><b>Hora de la sessió:</b> ".$columna['hora']."</li>";
                    echo "<li><b>Preu final:</b> ".$columna['precio']."</li></ol><br>";
                    


                }
                echo "</div>";
            }
        
    }
    

        

               
}

    ?>
    <br>
      <div  class='margin'>          
          <a class="btn" href="butaques.php">Enrere</a>
      </div>
</div>
    </body>
</html>