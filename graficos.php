<html>
    <head>
        <title>Estadisticas</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/colors.css">
        <link rel="stylesheet" type="text/css" href="css/estructura.css">
        <link rel="stylesheet" type="text/css" href="css/fonts.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    </head>
    <body class="borange">
    <div id="cos">
    <div id="titol">
                <h1>Estadistiques</h1>
        </div>
        <!-- Demana el dia de les estadistiques -->
        <div class="formulario">
        <form method="POST" action="graficos.php">    
            <b>Data:<b> <input type="date" name="data">
            <br>
            <br>
            <div class="margin">
            <input class="btn"  type="submit" name="s_afegir" value="Buscar">   
            </div> 
        </form>
</div>
        <!-- MySQL Agafar quantitat d'entrades comprades per sessio -->
        <?php

            $data = $_POST['data'];
    echo"<div  class='form_compra'>";
            echo "<h2>".$data."</h2><br><br>";

            require('database.php');

            $consulta = "SELECT s.id FROM entrada e 
            INNER JOIN sesiones s ON e.id_sesion = s.id 
            INNER JOIN pelicula p ON s.id_pelicula = p.id 
            WHERE s.fecha = '$data'";

            $resultado = mysqli_query($conexion, $consulta) or die ("Problemes en la consulta!");

            while($columna = mysqli_fetch_array($resultado)){            
                $num_sessio = $columna['id'];
            }

            $total_butaques = 120;
            $venudes = $resultado->num_rows;
            $per_vendre = $total_butaques - $venudes;

        ?>

        <!-- Pasar variables PHP a JavaScript -->
        <script type="text/javascript">
            venudes = <?= $venudes;?>;
            per_vendre = <?= $per_vendre;?>;
            console.log(venudes);
            console.log(per_vendre);
        </script>

        <!-- Mostra les estadistiques de sessions-->
        <div id="sessions"></div>
        </div>
        <script type="text/javascript">
            google.charts.load('current', {'packages':['corechart']});
            google.charts.setOnLoadCallback(drawChart);
            
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                ['Butaques', 'Session'],
                ['No Venudes',per_vendre],
                ['Venudes', venudes],
                ]);

                var options = {'width':550, 'height':400};
                var chart = new google.visualization.PieChart(document.getElementById('sessions'));
                chart.draw(data, options);
            }
        </script>


        <!-- Pelicules -->
        <!-- MySQL Agafar -->

        <?php
        
            require('database.php');

            $consulta = "SELECT id, nombre FROM pelicula";
            $resultado = mysqli_query($conexion, $consulta) or die ("Problemes en la consulta!");

            while($columna = mysqli_fetch_array($resultado)){
                $but[]=$columna['id'];
                $noms_per_peli[]=$columna['nombre'];
                
            }
            
            $cantidad_pelis = count($noms_per_peli);

            //print_r($noms_per_peli);
            //print_r($but);
        
        for($i=0; $i < count($but); $i++){
            $consulta = "SELECT p.id, p.nombre FROM entrada e 
            INNER JOIN sesiones s ON e.id_sesion = s.id 
            INNER JOIN pelicula p ON s.id_pelicula = p.id WHERE p.id = $but[$i]";

            $resultado = mysqli_query($conexion, $consulta) or die ("Problemes en la consulta!");

            $vendes_per_peli[] = $resultado->num_rows;
        }    

        ?>

        

        <!-- Pasar variables PHP a JavaScript -->
        <script type="text/javascript">

        let vendes_per_peli=<?php echo json_encode($vendes_per_peli);?>;
        let noms_per_peli=<?php echo json_encode($noms_per_peli);?>;   

        //console.log(vendes_per_peli);../
        //console.log(noms_per_peli);

        </script>

        <!-- Mostra les estadistiques de pelicules mes populars-->

         <script type="text/javascript">
            google.charts.load("current", {packages:['corechart']});
             google.charts.setOnLoadCallback(drawChart);
            
            function drawChart() {
                var data = google.visualization.arrayToDataTable([
                ["Element", "silver", { role: "style" } ],
                [noms_per_peli[1], vendes_per_peli[1], "red"],
                [noms_per_peli[2], vendes_per_peli[2], "blue"],
                [noms_per_peli[3], vendes_per_peli[3], "green"],
                [noms_per_peli[4], vendes_per_peli[4], "purple"],
                [noms_per_peli[5], vendes_per_peli[5], "yellow"],
                [noms_per_peli[6], vendes_per_peli[6], "silver"],
                [noms_per_peli[7], vendes_per_peli[7], "orange"],
                [noms_per_peli[8], vendes_per_peli[8], "white"],
            ]);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,{ calc: "stringify",sourceColumn: 1,type: "string",role: "annotation" },2]);

            var options = {
            title: "Entrades venudes per pelicula",
            width: 900,
            height: 400,
            bar: {groupWidth: "95%"},
            legend: { position: "none" },
            };
            var chart = new google.visualization.ColumnChart(document.getElementById("columnchart_values"));
            chart.draw(view, options);
        }
  </script>

    <div class="form_compra" id="columnchart_values" ></div>
    <br>
    <div  class='margin'>          
          <a class="btn" href="index.php">Inici</a>
    </div>
        </div>
    </body>
</html>