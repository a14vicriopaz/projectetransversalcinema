<?php
     
    $PHP_DIA = $_COOKIE["dia"];

    require_once 'database.php';
    $query = "SELECT * FROM sesiones s INNER JOIN pelicula p ON s.id_pelicula = p.id";
    $resultat = mysqli_query($conexion,$query);
    
    while($columna=mysqli_fetch_array($resultat)){

        $nom = $columna['nombre'];
        $fecha = $columna['fecha'];
        $imagen = $columna['imagen'];
        $horas_peli = $columna['hora'];
        $horas_peli = substr($horas_peli,0,-3); 

        $titles[$fecha]=$nom;
        $imgs[$fecha]=$imagen;
        $horas[$fecha]=$horas_peli;

    }

    //var_dump($titles);

?>

<script type="text/javascript">

let titles=<?php echo json_encode($titles);?>;
let imgs=<?php echo json_encode($imgs);?>;
let horas=<?php echo json_encode($horas);?>;

//console.log(horas);

</script>

<html>
    <head>
        <title>Cinema IAM</title>
        <meta charset="UTF 8">
        <link rel="stylesheet" type="text/css" href="css/estructuraPP.css?">
        <link rel="stylesheet" type="text/css" href="css/estructura.css">
        <link rel="stylesheet" type="text/css" href="css/colors.css">
        <link rel="stylesheet" type="text/css" href="css/fonts.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <link href="https://fonts.googleapis.com/css?family=Staatliches" rel="stylesheet">
        <script src="js/funcionesjs.js"></script>
    </head>
    <br>
    <br>
    <body class="borange">
        <div id="morado">
            <div id="cos">
                <div class="titol bwhite">
                    <h1>Cinema IAM</h1>
                </div> 
                <?php
                    $img1 = $imgs['2018-12-05'];
                    $img2 = $imgs['2018-12-14'];
                    $img3 = $imgs['2018-12-02'];
                    $img4 = $imgs['2018-12-20'];
                ?>
                <div class=box_pelis>
                    <img class="caratula" src="portades/<?=$img1;?>" alt="<?=$img1;?>">
                    <img class="caratula" src="portades/<?=$img2;?>" alt="<?=$img2;?>">
                    <img class="caratula" src="portades/<?=$img3;?>" alt="<?=$img3;?>">
                    <img class="caratula" src="portades/<?=$img4;?>" alt="<?=$img4;?>"> 
                </div>
                <div class="box_peli_selec">
                    <div class="box_titulo_pelicula">
                        <p id="titol_peli"></p>
                        <p id="hora_peli"></p>
                    </div>
                    <br>
                    <div class ="box_boto_enviar_dia">
                        <form action="butaques.php">
                        <button class="btn btn_long"> Comprar </button>
                        </form>
                    </div>
                </div>
                <div class="box_main_calendari">
                    <div class="print_calendari">
                    </div>
                    <!--
                    <div class="box_botons_mes">
                        <button class="boto_mes borange " onclick="resta();"> - </button>
                        <button class="boto_mes borange " onclick="sumar();"> + </button>
                    </div>
                    -->
                </div>
                <div class="box_anar_historial">
                     <a class="btn" href="historial.php">Historial</a>
                </div>
            </div>
        </div>
    </body>
</html>