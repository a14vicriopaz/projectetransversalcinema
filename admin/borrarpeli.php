<?php

$validar=0;
//comprovaciones

$nomErr = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {


    $nom = test_input($_POST["nom"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$nom)) {
      $nomErr = "No has posat el nom correctament."; 
      $validar++;
      
    }

}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

?>

<!DOCTYPE html>
<html lang="es">
<head>
  <title>FORMULARI</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="../css/colors.css">
  <link rel="stylesheet" type="text/css" href="../css/estructura.css">
  <link rel="stylesheet" type="text/css" href="../css/fonts.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="js/funcionesbut.js"></script>

</head>
<body class="borange">
<div id="cos">
  <div id="titol">
    <h1>Borrar peliculila</h1>
</div>
<div class="formulario">
    <form method="POST" action="<?php echo $_SERVER['PHP_SELF']; ?>">

    <b>Introdueix el nom de la pel.licula:</b><br>
    <br>

    <b>Nom:</b> <input type="text" name="nom" required>
    <span class="error"> <?php echo $nomErr;?></span>
    <br><br>
    
    <input type="submit" name="borrar" value="BORRAR">    
    
    
    </form>
</div>
    <?php

    $nom = $_POST['nom'];


    require('../database.php');
    

    if(isset($_POST['borrar'])){  

        //Comprobar que la peli exista

      if($validar>=1){

        echo "Les dades no son correctes!";

      }else{

        $sql="SELECT nombre FROM pelicula WHERE  nombre = '$nom'";

        if(mysqli_query($conexion, $sql)){

          if(empty($result)){

            $sql="DELETE FROM pelicula WHERE  nombre = '$nom'";
            $result = mysqli_query($conexion, $sql) or die ("Problemes en la consulta!");


          }else{
            
            echo "Aquesta pel.licula ja existeix!";
            
          }
  
        }else{
          
          echo "Error: " .$sql . "<br>" . mysqli_error($conexion);
        }

      mysqli_close($conexion);
    }
  }

    ?>

    <br>
    <div class="margin">
    <a class="btn" href="admin.php">Enrere</a>
    </div>
    
</div>
    </body>
</html>
