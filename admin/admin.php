<?php

function validar_fecha_espanol($fecha){
	$valores = explode('/', $fecha);
	if(count($valores) == 3 && checkdate($valores[1], $valores[0], $valores[2])){
		return true;
    }
	return false;
}

$validar=0;
//comprovaciones

$genereErr = $nomErr = $sipnosisErr = $duracioErr= "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {

  if(empty($_POST['id_peli'])){

    if (empty($_POST["name"])) {
      $nameErr = "Name is required";
    } else {
      $nom = test_input($_POST["nom"]);
      
      if (!preg_match("/^p{L}+$/ui",$nom)) {
        $nomErr = "No has posat el nom de la película correctament."; 
        $validar++;
      }
    }
  
    $genere = test_input($_POST["genere"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$genere)) {
        $genereErr = "No has posat el genere correctament."; 
      $validar++;
    }

    /*$sipnosis = test_input($_POST["sipnosis"]);
    if (!preg_match("/^p{L}+$/ui",$sipnosis)) {
        $sipnosisErr = "No has posat el sipnosis correctament."; 
      $validar++;
    }
*/
    $duracio = test_input($_POST["duracio"]);
    if(!preg_match("/^[0-9]{3}$/", $duracio)) {

        $duracioErr = "No has posat el duracio correctament.";
      $validar++;

    }
  }else{

    $id_peli = test_input($_POST["id_peli"]);
    if(empty($_POST['id_peli'])){
      $nom_peliErr = "No has posat el nom de la película correctament."; 
      $validar++;
      
    }
    if(validar_fecha_espanol($_POST["data"])){

      $dataErr = "No has posat la data correctament.";
      $validar++;

    }

    if ((($_POST['d_esp'])!=1) && (($_POST['d_esp'])!=0)){

      $diaErr = "No has escollit un camp correctament.";
      $validar++;
    }
    if ((($_POST['d_vip'])!=1) && (($_POST['d_vip'])!=0)){

      $vipErr = "No has escollit un camp correctament.";
      $validar++;
    }
    //la hora no cal validarla.

  }

}

function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
  }

?>

<!DOCTYPE html>
<html lang="es">
<head>
  <title>ADMINISTRADOR</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="../css/colors.css">
  <link rel="stylesheet" type="text/css" href="../css/estructura.css">
  <link rel="stylesheet" type="text/css" href="../css/fonts.css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
  <script src="js/funcionesbut.js"></script>
  

 <!-- <script>
    $(document).ready(function() {

      $( "#correu" ).focus();

      $( "#hide_pelicula" ).hide();
      $( "#hide_sessio" ).hide();

      $( "#pelicula" ).click(function(){
		      $( "#hide_pelicula" ).toggle();

	});
  $( "#sessio" ).click(function(){
		      $( "#hide_sessio" ).toggle();

	});


    });
  </script>-->
</head>
<body class="borange">
<!-- Formulario Peliculas-->
<div id="cos">
  <div id="titol">
    <h1>ADMINISTRACIÓ DEL CINEMA</h1>
  </div>
  <div id="f_admin_box">
  <div class="f_admin tamano" id="f_ad_p">
    <form method="POST" id="fpelicula" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <label for="pelicula">AFEGIR UNA PEL.LÍCULA:</label>
    <br>
    <br>
    <table>
      <tr>
        <td>Nom pelicul·la:</td>  
        <td><input type="text" name="nom"></td> 
          <span class="error"> <?php echo $nomErr;?></span>
      </tr>
      <tr>
       <td> Duració: </td>
        <td><input type="text" name="duracio"  ></td>
      </tr>
          <span class="error"> <?php echo $duracioErr;?></span>
      <tr>
        <td>Genere: </td>
        <td><input type="text" name="genere"  ></td>
      </tr>
          <span class="error"> <?php echo $genereErr;?></span>
      <tr>
        <td>Edad minima: </td>
       <td><input type="text" name="edad"  ></td>
      </tr>
          <span class="error"> <?php echo $edadErr;?></span>
      <tr>
        <td>Imatge: </td>
        <td><input type="text" name="imagen"  ></td>
      </tr>
          <span class="error"> <?php echo $imagenErr;?></span>
      <tr>
        <td>Sinopsis:</td>
        <td> <textarea type="text" name="sinopsis"  ></textarea></td>
      </tr>
          <span class="error"> <?php echo $genereErr;?></span>
    </table>
    <br>
    <input class="b_admin " type="submit" name="p_afegir" value="AFEGIR">   
    </form>
  </div>
<!-- Formulario SESSIONES-->
<div class="f_admin tamano" id="f_ad_s">
    <form method="POST" id="fsession" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <label for="sessio">AFEGIR UNA SESSIÓ:</label>
    <br><br>
    
    Data: <input type="date" name="data">
    <span class="error"> <?php echo $dataErr;?></span>
    <br><br>

    Hora: <input type="time" name="hora">
    <br><br>
    Id pelicul·la: <input type="text" name="id_peli"  >
    <span class="error"> <?php echo $nom_peliErr;?></span>
    <br><br>
    
    <label>Serà dia de l'espectador ?
    <input type="radio" name="d_esp" value="1">SI
    <input type="radio" name="d_esp" value="0" checked="checked">NO
    
  <span class="error"> <?php echo $diaErr;?></span>
  <br><br>

  <label>Hi hauràn butaques VIP ?
    <input type="radio" name="d_vip" value="1">SI
    <input type="radio" name="d_vip" value="0" checked="checked">NO
  <span class="error"> <?php echo $vipErr;?></span>
  <br>
  <br>
    <input class="b_admin"  type="submit" name="s_afegir" value="AFEGIR">    
    
    
    </form>
  </div>  
  </div>  
  <br>
  <div id="b_borrar">
    <button class="btn" id="borrar_peli" onclick="location.href='borrarpeli.php'">Borrar Pelicula</button>
    <button class="btn" id="borrar_sessio" onclick="location.href='borrarsessio.php'">Borrar Session</button>
      </div>
  <div id="consultes">
  <?php

require('../database.php');

    //Mostra pel.licules

    $consulta = "SELECT id, nombre FROM pelicula";
    $resultado = mysqli_query($conexion, $consulta) or die ("Problemes en la consulta!");
    
    echo "<div class='tamano' id='p_registrades'><h2>Pel.licules registrades:</h2>";
    echo"<table>";
    while($columna = mysqli_fetch_array($resultado)){
      echo"<tr>";
    echo "<td><b>ID :</b> ".$columna['id']."</td>";
    echo"<td><b>Nom :</b> ".$columna['nombre']."</td>";
    echo"</tr>";
    }
    echo"</table>";
    echo "</div>";

    //Mostra sessions

    $consulta2 = "SELECT id, fecha, hora FROM sesiones";
    $resultado2 = mysqli_query($conexion, $consulta2) or die ("Problemes en la consulta!");
    
    echo "<div class='tamano' id='s_registrades'><h2>Sessions registrades:</h2>";
    echo"<table>";
    while($columna2 = mysqli_fetch_array($resultado2)){
    echo"<tr>";
    echo "<td><b>ID: </b>".$columna2['id']."</td>";
      echo "<td><b>Data: </b>".$columna2['fecha']."</td>";
      echo"<td><b>Hora: </b>".$columna2['hora']."</td>";
    echo"</tr>";
    }
    echo"</table>";
    echo "</div>";
   
    ?>
 </div>
    <?php

    //insertar datos!

    $nom = $_POST['nom'];
    $duracio = $_POST['duracio'];
    $genere = $_POST['genere'];
    $edad_min = $_POST['edad'];
    $imagen = $_POST['imagen'];
    $sinopsis=$_POST['sinopsis'];

    $id_pelicula = $_POST['id_peli'];
    $data = $_POST['data'];
    $hora = $_POST['hora'];
    $dia=$_POST['d_esp'];
    $vip=$_POST['d_vip'];

    require('../database.php');

    if(isset($_POST['p_afegir'])){

      if($validar>=1){

        echo "Les dades no son correctes!";

      }else{

        echo "tot correcte!";

        $sql1= "INSERT INTO pelicula (nombre, imagen, sinopsis, duracion, genero, edad_min) VALUES 
            ('$nom', '$imagen', '$sinopsis', '$duracio', '$genere', '$edad_min')";

            if(mysqli_query($conexion, $sql1)){

              echo "New record created successfully!";

            }else{
        
              echo "Error: " .$sql1 . "<br>" . mysqli_error($conexion);
            }
          }
        }



        if(isset($_POST['s_afegir'])){

          if($validar>=1){
    
            echo "Les dades no son correctes!";
    
          }else{
            $sql2= "INSERT INTO sesiones (fecha, hora, id_pelicula, fila_vip, dia_espectador) 
            VALUES ('$data', '$hora', $id_pelicula, '$vip', '$dia')";

            if(mysqli_query($conexion, $sql2)){

              echo "New record created successfully!";

            }else{
        
              echo "Error: " .$sql2. "<br>" . mysqli_error($conexion);
            }
          }
        }

     

    ?>   
    
      </div>
    </body>
</html>