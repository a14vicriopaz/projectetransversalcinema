
function validacion() {

    valor = document.getElementById("nom").value;
    valor2 = document.getElementById("cognoms").value;
    valor3 = document.getElementById("telefon").value;

if (valor == null || valor.length == 0 || /^\s+$/.test(valor)) {
  swal({
    type: 'error',
    title: 'Oops...',
    text: 'El nom no es correcte',
    footer: '<a href>Why do I have this issue?</a>'
  })
  return false;
  
}
else if (valor2 == null || valor2.length == 0 || /^\s+$/.test(valor2)) {

  swal({
    type: 'error',
    title: 'Oops...',
    text: 'Els cognoms no són correcte',
    footer: '<a href>Why do I have this issue?</a>'
  })
  return false;
  
}
else if (!(/^\d{9}$/.test(valor3))) {
  
  swal({
    type: 'error',
    title: 'Oops...',
    text: 'El telefon no es correcte',
    footer: '<a href>Why do I have this issue?</a>'
  })
  return false;
}else{

return true;
}  
}
