$(document).ready(function () {
    
    for(let i=0; i<contbut;i++){

        $(ndbutaca[i]).removeClass("disponible").addClass("NoDisponible");
    }
    
    var cont = 0;
    var total = 0;
    var p_total = 0;
    var seleccionades = [];
    vipcount=0;
    butacacount=0;
    $("td").click(function () {
        var butaca = $(this).children();
        if (butaca.hasClass("selecionat") && cont > 0) {
            cont = cont - 1;
        }
        if (cont <= 9) {
            if (butaca.hasClass("disponible")) {
                butaca.fadeOut(100);
                let id = $(this).children().attr("id");
                butaca.removeClass("disponible");
                butaca.fadeIn(100);
                butaca.addClass("selecionat");
                cont++;

                seleccionades[cont] = id;
                if(dia_es==1){
                    if(butaca.hasClass("selecionat")){
                        if(butaca.hasClass("vip") ){
                            preu = 6;
                            p_total = p_total + preu;
                            vipcount++;
                        }else{
                            preu = 4;
                            p_total = p_total + preu;
                            butacacount++;
                        }

                    }else{
        
                    }
                }else{
                    if(butaca.hasClass("selecionat")){
                        if(butaca.hasClass("vip")){
                            preu = 8;
                            p_total = p_total + preu;
                            vipcount++;
                        }else{
                            preu = 6;
                            p_total = p_total + preu;
                            butacacount++;
                        }
                    }else{
        
                    }
                }

            } else if (butaca.hasClass("NoDisponible")) {
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Aquesta butaca ja esta ocupada',
                    footer: '<a href>Why do I have this issue?</a>'
                  })
            } else {
                butaca.fadeOut(100);
                butaca.removeClass("selecionat");
                butaca.fadeIn(100);
                butaca.addClass("disponible");
                if(dia_es==1){
                    if(butaca.hasClass("disponible")){
                        if(butaca.hasClass("vip") ){
                            preu = 6;
                            p_total = p_total - preu;
                            vipcount--;
                        }else{
                            preu = 4;
                            p_total = p_total - preu;
                            butacacount--;
                        }

                    }else{
        
                    }
                }else{
                    if(butaca.hasClass("disponible")){
                        if(butaca.hasClass("vip")){
                            preu = 8;
                            p_total = p_total - preu;
                            vipcount--;
                        }else{
                            preu = 6;
                            p_total = p_total - preu;
                            butacacount--;
                        }
                    }else{
        
                    }
                }
                var index = seleccionades.indexOf($(this).children().attr("id"));
                if (index > -1) {
                    seleccionades.splice(index, 1);
                }
            }
        } else {
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'No es poden seleccionar més butaques',
                footer: '<a href>Why do I have this issue?</a>'
              })
        }
        
        document.getElementById("b_s").innerHTML = seleccionades;
        document.getElementById("n_s").innerHTML = cont;
        document.getElementById("c_f").innerHTML = p_total+" €";

    });
    $("#boton2").click(function (event) {
        if(cont<=0){
            event.preventDefault();
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'No has seleccionat cap butaca',
                footer: '<a href>Why do I have this issue?</a>'
              })
        }else{
            document.cookie = "cbutaca="+seleccionades;
            document.cookie = "ptotal="+p_total;
            document.cookie = "countvip="+vipcount;
            document.cookie = "countbutaca="+butacacount;
            document.cookie = "t_butaca="+cont;
        }
    });
});
