/* Calendari */

let data = new Date();
let month = data.getMonth()+1;
let year = data.getFullYear();
let actual_month = data.getMonth()+1;
let actual_year = data.getFullYear();

// Incrementa o disminueix el mes.
function sumar() {
    month++
    let printmonth = month;
    if (month==13) {
        month=1;
        year++;
    }

    $(document).ready(function(){
        let laTabla= fer_cal(month, year);
        //console.log(laTabla);
        $(".print_calendari").html(laTabla);

    });
}

function resta() {
    month--
    let printmonth = month;
    if (month==0) {
        month=12;
        year--;
    }

    $(document).ready(function(){
        let laTabla= fer_cal(month, year);
        //console.log(laTabla);
        $(".print_calendari").html(laTabla);

    });
}

// Realitza el calendari
function fer_cal(month, year){

    // contador de dies per al mes actual / sempre 1.
    let contador = 1;
    //contador dies abans del mes actual / sempre 1.
    let cont= 1;

    // Retorna la cuantitat de dies que hi ha en aquest mes.

    function daysInMonth (month, year) {
        return new Date(year, month, 0).getDate();
    }

    // Pasar a variable la funcio quantita de dies en el mes +1.
    let max_mes = daysInMonth(month, year)+1;
    // Indica quin dia de la setmana comença el mes.
    let dies_en_blanc = new Date (year,month-1,1).getDay();
    // Dia actual
    let dia_actual = data.getDate();

    // Calendari.
       
        let laTabla="";

        laTabla+="<div><table>";
        laTabla+=("<td class ='dia'>Dilluns</td><td class ='dia'>Dimarts</td><td class ='dia'>Dimecres</td><td class ='dia'>Dijous</td><td class ='dia'>Divendres</td><td class ='dia'>Dissabte</td><td class ='dia'>Diumenge</td>");

        for (let a=1; a <= 6; a++){
            laTabla+="<tr>";
            for (let b=1; b <= 7; b++){
                if (contador == max_mes){
                }
                else{
                    if (dies_en_blanc > cont){
                        laTabla+="<td class = 'mes_anterior'></td>";
                        cont++;
                    }
                    else{

                        if(contador<10){
                            contador="0"+contador;
                        }

                        let data_ambcont = actual_year+"-"+month+"-"+contador;

                        if (titles[data_ambcont] == null){
                            //console.log("empty");
                            laTabla+="<td data="+data_ambcont+" class='cal_boto mes_actual'>"+ contador + "</td>";
                            contador++;
                        }else{
                            //console.log("lleno");
                            laTabla+="<td data="+data_ambcont+" class='cal_boto dia_pelicula mes_actual'>"+ contador + "</td>";
                            contador++;
                        }
                        
                    }
                }
            }

            laTabla+="</tr>";
        }
        laTabla+="</table></div>";
        return laTabla;
    }

/* MOSTRAR CAL A PAGINA PRINCIPAL */

$(document).ready(function(){

    // Actualitza el calendari.
    let laTabla= fer_cal(month, year);
    //console.log(laTabla);
    $(".print_calendari").html(laTabla);

});

/* FI MOSTRAR CAL A PAGINA PRINCIPAL */

/* FI CALENDARI */

/* INFORMACIO PELI */

$(document).ready(function(){
    
    // Mostrar i amagar les hores
    $(".box_peli_selec").hide();
    $(".box_peli_selec").attr("id","ocult");
    
   
    // Agafar dia des de tr i pasarlo por cookie 
    $(".print_calendari").on("click",".dia_pelicula",function(){

        // Agafo el atribut del tr
        let data_actual = $(this).attr("data");
        //console.log(data_actual);
        
        // Enviar al html
        $("#dia").html(data_actual);

        // Envia el nom de la pelicula al html
        $("#titol_peli").html(titles[data_actual]);

        // Envia la hora de la pelicula al html
        $("#hora_peli").html(horas[data_actual]);

        // Paso per cookie el valor data_actual
        document.cookie = "dia="+data_actual;

        // Mostrar informacio del dia selecionat
        if($(".box_peli_selec").attr("id")=="ocult"){
            $(".box_peli_selec").show();
            $(".box_peli_selec").attr("id","actiu");

        }

        // Posar color al dia selecionat.
        let col_boto ="dia_actiu";

        $(".dia_pelicula").removeClass(col_boto);

        if ($(this).hasClass()==col_boto){
            $(this).removeClass(col_boto);
        }else{
            $(this).addClass(col_boto);
        }

        // Amagar el boto de compra si el dia ha passat.
        let dia_seleccionat = $(this).html();
        console.log(dia_seleccionat);

        let dia_actual = data.getDate();

        if (dia_seleccionat < dia_actual){
            $(".btn_long").hide();
        }else{
            $(".btn_long").show();
        }

    })
});

/* FI INFORMACIO PELI */ 