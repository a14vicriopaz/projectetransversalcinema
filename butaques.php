<!DOCTYPE html>
<html>

<head>
    <title>Seleció de butaques</title>
    <meta charset="latin1_spanish_ci">
    <link rel="stylesheet" type="text/css" href="css/colors.css">
    <link rel="stylesheet" type="text/css" href="css/estructura.css">
    <link rel="stylesheet" type="text/css" href="css/fonts.css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <script src="js/funcionesbut.js"></script>
</head>

<body class="borange">
    
    <?php
    $contbo = 0;
    $data = $_COOKIE['dia'];
    require_once 'database.php';
    $query = "SELECT e.butaca FROM entrada e INNER JOIN sesiones s ON e.id_sesion=s.id INNER JOIN pelicula p ON p.id=s.id_pelicula WHERE s.fecha='$data'";
    $resultat = mysqli_query($conexion,$query);
    ?>
    <script type="text/javascript">
        let ndbutaca = [];
    <?php
    while($row=mysqli_fetch_array($resultat)){
        ?>
        ndbutaca.push("<?='#'.$row['butaca']; ?>");
    <?php
    $contbo++;
    }
    echo $contbo;
    ?>

    let contbut = <?php echo $contbo; ?>
    </script>

<!--NEW-->
<?php
    $data = $_COOKIE['dia'];
    require_once 'database.php';
    $query = "SELECT dia_espectador FROM sesiones WHERE fecha='$data'";
    $resultat = mysqli_query($conexion,$query);
    ?>
   
    <script type="text/javascript">
    <?php
    while($row=mysqli_fetch_array($resultat)){
        ?>
    let dia_es = (<?=$row['dia_espectador']; ?>);
    <?php
    }
    ?>
    </script>
    <!--NEW-->
        <div id="cos">
            <div id="titol">
            <?php
                $data = $_COOKIE['dia'];
                $dia = substr($data,8,10);
                $mes = substr($data,5,-3);
                $any = substr($data,0,-6);
                $data = $dia." / ".$mes." / ".$any;
                echo "<h1> Sala de sessió: ".$data."</h1>";
            ?>
            </div>

            <div id="taula">
                <table id="taulaButaques">
                    <tr>
                        <td> <img id="b01" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b02" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b03" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b04" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b05" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b06" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b07" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b08" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b09" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b10" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b11" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b12" class="butaca disponible" src="imgs/butaca.png"> </td>
                    </tr>
                    <tr>
                        <td> <img id="b13" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b14" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b15" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b16" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b17" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b18" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b19" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b20" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b21" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b22" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b23" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b24" class="butaca disponible" src="imgs/butaca.png"> </td>
                    </tr>
                    <tr>
                        <td> <img id="b25" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b26" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b27" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b28" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b29" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b30" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b31" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b32" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b33" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b34" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b35" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b16" class="butaca disponible" src="imgs/butaca.png"> </td>
                    </tr>
                    <tr>
                        <td> <img id="b37" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b38" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b39" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b40" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b41" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b42" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b43" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b44" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b45" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b46" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b47" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b48" class="butaca disponible" src="imgs/butaca.png"> </td>
                    </tr>

                    <?php
                        $data = $_COOKIE['dia'];
                        require_once 'database.php';
                        $query = "SELECT * FROM sesiones WHERE fecha='$data'";
                        $resultat = mysqli_query($conexion,$query);
                        while($row=mysqli_fetch_array($resultat)){
                        if($row['fila_vip']==1){
                        echo "<tr>";
                            echo "<td> <img id='b49' class='butaca disponible vip' src='imgs/butacavip.png'> </td>";
                            echo "<td> <img id='b50' class='butaca disponible vip' src='imgs/butacavip.png'> </td>";
                            echo "<td> <img id='b51' class='butaca disponible vip' src='imgs/butacavip.png'> </td>";
                            echo "<td class='espai'></td>";
                            echo "<td> <img id='b52' class='butaca disponible  vip' src='imgs/butacavip.png'> </td>";
                            echo "<td> <img id='b53' class='butaca disponible  vip' src='imgs/butacavip.png'> </td>";
                            echo "<td> <img id='b54' class='butaca disponible  vip' src='imgs/butacavip.png'> </td>";
                            echo "<td> <img id='b55' class='butaca disponible vip' src='imgs/butacavip.png'> </td>";
                            echo "<td> <img id='b56' class='butaca disponible vip' src='imgs/butacavip.png'> </td>";
                            echo "<td> <img id='b57' class='butaca disponible vip' src='imgs/butacavip.png'> </td>";
                            echo "<td class='espai'></td>";
                            echo "<td> <img id='b58' class='butaca disponible vip' src='imgs/butacavip.png'> </td>";
                            echo "<td> <img id='b59' class='butaca disponible vip' src='imgs/butacavip.png'> </td>";
                            echo "<td> <img id='b60' class='butaca disponible vip' src='imgs/butacavip.png'> </td>";
                        echo "</tr>";
                        }else{
                         echo "<tr>";
                            echo "<td> <img id='b49' class='butaca disponible' src='imgs/butaca.png'> </td>";
                            echo "<td> <img id='b50' class='butaca disponible' src='imgs/butaca.png'> </td>";
                            echo "<td> <img id='b51' class='butaca disponible' src='imgs/butaca.png'> </td>";
                            echo "<td class='espai'></td>";
                            echo "<td> <img id='b52' class='butaca disponible' src='imgs/butaca.png'> </td>";
                            echo "<td> <img id='b53' class='butaca disponible' src='imgs/butaca.png'> </td>";
                            echo "<td> <img id='b54' class='butaca disponible' src='imgs/butaca.png'> </td>";
                            echo "<td> <img id='b55' class='butaca disponible' src='imgs/butaca.png'> </td>";
                            echo "<td> <img id='b56' class='butaca disponible' src='imgs/butaca.png'> </td>";
                            echo "<td> <img id='b57' class='butaca disponible' src='imgs/butaca.png'> </td>";
                            echo "<td class='espai'></td>";
                            echo "<td> <img id='b58' class='butaca disponible' src='imgs/butaca.png'> </td>";
                            echo "<td> <img id='b59' class='butaca disponible' src='imgs/butaca.png'> </td>";
                            echo "<td> <img id='b60' class='butaca disponible' src='imgs/butaca.png'> </td>";
                         echo "</tr>";
                        }
                    }
                    ?>
                    <tr>
                        <td> <img id="b61" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b62" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b63" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b64" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b65" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b66" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b67" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b68" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b69" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b70" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b71" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b72" class="butaca disponible" src="imgs/butaca.png"> </td>
                    </tr>
                    <tr>
                        <td> <img id="b73" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b74" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b75" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b76" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b77" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b8" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b79" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b80" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b81" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b82" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b83" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b84" class="butaca disponible" src="imgs/butaca.png"> </td>
                    </tr>
                    <tr>
                        <td> <img id="b85" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b86" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b87" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b88" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b89" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b90" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b91" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b92" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b93" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b94" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b95" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b96" class="butaca disponible" src="imgs/butaca.png"> </td>
                    </tr>
                    <tr>
                        <td> <img id="b97" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b98" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b99" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b100" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b101" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b102" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b103" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b104" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b105" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b106" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b107" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b108" class="butaca disponible" src="imgs/butaca.png"> </td>
                    </tr>
                    <tr>
                        <td> <img id="b109" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b110" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b111" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b112" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b113" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b114" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b115" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b116" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b117" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td class="espai"></td>
                        <td> <img id="b118" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b119" class="butaca disponible" src="imgs/butaca.png"> </td>
                        <td> <img id="b120" class="butaca disponible" src="imgs/butaca.png"> </td>
                    </tr>


                </table>
            </div>
            <div id="descripcio">
                <p>Descripció</p>
                    <?php
                     $data = $_COOKIE['dia'];
                        require_once 'database.php';
                        $query = "SELECT * FROM pelicula p INNER JOIN sesiones s WHERE s.id_pelicula=p.id AND s.fecha='$data'";
                        $resultat = mysqli_query($conexion,$query);
                        while($columna=mysqli_fetch_array($resultat)){
                            echo "<p class='min'><b>Nom:</b> ".$columna['nombre']."</p>";
                            echo "<p class='min'><b>Sinopsis:</b> ".$columna['sinopsis']."</p>";
                            echo "<p class='min'><b>Duració:</b> ".$columna['duracion']."</p>";
                            echo "<p class='min'><b>Génere:</b> ".$columna['genero']."</p>";
                            echo "<p class='min'><b>Edat mínima:</b> ".$columna['edad_min']."</p>";
                        }
?>
            </div>
            <div id="menu">
                <table>
                    <tr>
                        <td><img width="30px" height="30px" src="imgs/butaca.png"> </td>
                        <td>Butaques lliures</td>
                        <td><img width="30px" height="30px" src="imgs/butacavip.png"></td>
                        <td>Butaques VIP</td>
                    </tr>
                    <tr>
                        <td id="grey"><img width="30px" height="30px" src="imgs/butaca.png"></td>
                        <td>Butaques ocupades</td>
                    </tr>
                </table>
            </div>
            <div id="dades">
                <p> Butaques seleccionades:</p>
                <p id="b_s"></p>
                <p> Total seleccionades:</p>
                <p id="n_s"></p>
                <p> Preu total:</p>
                <p id="c_f"></p>
            </div>
            <br>
            <div id="botones">
            
           
                <form method="post" action="correu.php" >
                <button class="btn bblue black dreta" type="submit" id="boton2" >Comprar</button>
                <a class="btn bblue black esquerra" href="index.php">Enrere</a>
                </form>
               
            </div>
     
                

        </div>

</body>

</html>
