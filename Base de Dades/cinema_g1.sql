-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Temps de generació: 20-12-2018 a les 10:20:00
-- Versió del servidor: 5.7.24-0ubuntu0.16.04.1
-- Versió de PHP: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de dades: `cinema_g1`
--

-- --------------------------------------------------------

--
-- Estructura de la taula `entrada`
--

CREATE TABLE `entrada` (
  `id` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `butaca` varchar(4) NOT NULL,
  `id_sesion` int(11) NOT NULL,
  `correo` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcant dades de la taula `entrada`
--

INSERT INTO `entrada` (`id`, `precio`, `butaca`, `id_sesion`, `correo`) VALUES
(1, 5, 'b01', 1, 'cvila@gmail.com'),
(2, 5, 'b06', 5, 'joan@gmail.com'),
(3, 5, 'b04', 2, 'victor@gmail.com'),
(4, 5, 'b02', 1, 'cvila@gmail.com'),
(6, 5, 'b02', 3, 'cvila@gmail.com'),
(7, 5, 'b06', 4, 'joan@gmail.com'),
(8, 3, 'b67', 2, 'cvila@gmail.com'),
(9, 6, 'b115', 2, 'mbelen@gmail.com'),
(10, 6, 'b116', 2, 'mbelen@gmail.com'),
(11, 6, 'b76', 2, 'mabe@gmail.com'),
(12, 6, 'b77', 2, 'mabe@gmail.com'),
(13, 6, 'b29', 2, 'albaro@iam.cat'),
(14, 6, 'b31', 2, 'albaro@iam.cat'),
(15, 6, 'b42', 2, 'albaro@iam.cat'),
(16, 6, 'b18', 2, 'albaro@iam.cat'),
(17, 6, 'b53', 2, 'albaro@iam.cat'),
(18, 6, 'b55', 2, 'albaro@iam.cat'),
(19, 6, 'b66', 2, 'albaro@iam.cat'),
(20, 6, 'b80', 2, 'albaro@iam.cat'),
(21, 6, 'b44', 2, 'albaro@iam.cat'),
(22, 6, 'b20', 2, 'albaro@iam.cat'),
(23, 6, 'b90', 2, 'adria@iam.cat'),
(24, 6, 'b88', 2, 'adria@iam.cat'),
(25, 6, 'b79', 2, 'adria@iam.cat'),
(26, 6, 'b92', 2, 'adria@iam.cat'),
(27, 6, 'b81', 2, 'adria@iam.cat'),
(28, 6, 'b91', 2, 'adria@iam.cat'),
(29, 6, 'b8', 2, 'adria@iam.cat'),
(30, 6, 'b89', 2, 'adria@iam.cat'),
(31, 6, 'b93', 2, 'adria@iam.cat'),
(32, 6, 'b68', 2, 'adria@iam.cat'),
(33, 6, 'b57', 8, 'francesc@iam.cat'),
(34, 6, 'b56', 8, 'francesc@iam.cat'),
(35, 6, 'b52', 8, 'francesc@iam.cat'),
(36, 6, 'b53', 8, 'francesc@iam.cat'),
(37, 6, 'b50', 8, 'francesc@iam.cat'),
(38, 6, 'b51', 8, 'francesc@iam.cat'),
(39, 6, 'b49', 8, 'francesc@iam.cat'),
(40, 6, 'b58', 8, 'francesc@iam.cat'),
(41, 6, 'b59', 8, 'francesc@iam.cat'),
(42, 6, 'b60', 8, 'francesc@iam.cat'),
(43, 6, 'b43', 8, 'oriol@iam.cat'),
(44, 6, 'b42', 8, 'oriol@iam.cat'),
(45, 6, 'b66', 8, 'oriol@iam.cat'),
(46, 6, 'b67', 8, 'oriol@iam.cat'),
(47, 6, 'b8', 8, 'oriol@iam.cat'),
(48, 6, 'b79', 8, 'oriol@iam.cat'),
(49, 6, 'b90', 8, 'oriol@iam.cat'),
(50, 6, 'b91', 8, 'oriol@iam.cat'),
(51, 6, 'b102', 8, 'oriol@iam.cat'),
(52, 6, 'b103', 8, 'oriol@iam.cat'),
(63, 6, 'b112', 12, 'gerard@gmail.com'),
(64, 6, 'b101', 12, 'gerard@gmail.com'),
(65, 6, 'b90', 12, 'gerard@gmail.com'),
(66, 6, 'b79', 12, 'gerard@gmail.com'),
(67, 6, 'b68', 12, 'gerard@gmail.com'),
(68, 6, 'b57', 12, 'gerard@gmail.com'),
(69, 6, 'b44', 12, 'gerard@gmail.com'),
(70, 6, 'b31', 12, 'gerard@gmail.com'),
(71, 6, 'b18', 12, 'gerard@gmail.com'),
(72, 6, 'b05', 12, 'gerard@gmail.com'),
(73, 6, 'b28', 12, 'eduard@gmail.com'),
(74, 6, 'b41', 12, 'eduard@gmail.com'),
(75, 6, 'b54', 12, 'eduard@gmail.com'),
(76, 6, 'b66', 12, 'eduard@gmail.com'),
(77, 6, 'b77', 12, 'eduard@gmail.com'),
(78, 6, 'b88', 12, 'eduard@gmail.com'),
(79, 6, 'b64', 12, 'eduard@gmail.com'),
(80, 6, 'b115', 12, 'eduard@gmail.com'),
(81, 6, 'b104', 12, 'eduard@gmail.com'),
(82, 6, 'b93', 12, 'eduard@gmail.com'),
(83, 6, 'b30', 4, 'marc@gmail.com'),
(84, 6, 'b54', 4, 'marc@gmail.com'),
(85, 6, 'b8', 4, 'marc@gmail.com'),
(86, 6, 'b102', 4, 'marc@gmail.com'),
(87, 6, 'b104', 4, 'marc@gmail.com'),
(88, 6, 'b80', 4, 'marc@gmail.com'),
(89, 6, 'b56', 4, 'marc@gmail.com'),
(90, 6, 'b32', 4, 'marc@gmail.com'),
(91, 6, 'b28', 4, 'marc@gmail.com'),
(92, 6, 'b52', 4, 'marc@gmail.com'),
(93, 6, 'b04', 10, 'joan_vila@iam.cat'),
(94, 6, 'b05', 10, 'joan_vila@iam.cat'),
(95, 6, 'b06', 10, 'joan_vila@iam.cat'),
(96, 6, 'b07', 10, 'joan_vila@iam.cat'),
(97, 6, 'b08', 10, 'joan_vila@iam.cat'),
(98, 6, 'b09', 10, 'joan_vila@iam.cat'),
(99, 6, 'b30', 10, 'carlos@gmail.com'),
(100, 6, 'b43', 10, 'carlos@gmail.com'),
(101, 6, 'b54', 10, 'carlos@gmail.com'),
(102, 6, 'b67', 10, 'carlos@gmail.com'),
(103, 6, 'b8', 10, 'carlos@gmail.com'),
(104, 6, 'b91', 10, 'carlos@gmail.com'),
(105, 6, 'b102', 10, 'carlos@gmail.com'),
(106, 6, 'b115', 10, 'carlos@gmail.com'),
(107, 6, 'b19', 10, 'carlos@gmail.com'),
(108, 6, 'b104', 10, 'carlos@gmail.com'),
(109, 6, 'b50', 6, 'juan_jose@gmail.com'),
(110, 6, 'b59', 6, 'juan_jose@gmail.com'),
(111, 6, 'b53', 6, 'juan_jose@gmail.com'),
(112, 6, 'b56', 6, 'juan_jose@gmail.com'),
(113, 6, 'b8', 6, 'juan_jose@gmail.com'),
(114, 6, 'b79', 6, 'juan_jose@gmail.com'),
(115, 6, 'b103', 6, 'juan_jose@gmail.com'),
(116, 6, 'b102', 6, 'juan_jose@gmail.com'),
(117, 6, 'b30', 6, 'juan_jose@gmail.com'),
(118, 6, 'b31', 6, 'juan_jose@gmail.com'),
(119, 6, 'b23', 4, 'marian@gmail.com'),
(120, 6, 'b47', 4, 'marian@gmail.com'),
(121, 6, 'b71', 4, 'marian@gmail.com'),
(122, 6, 'b95', 4, 'marian@gmail.com'),
(123, 6, 'b119', 4, 'marian@gmail.com'),
(124, 6, 'b110', 4, 'marian@gmail.com'),
(125, 6, 'b86', 4, 'marian@gmail.com'),
(126, 6, 'b62', 4, 'marian@gmail.com'),
(127, 6, 'b38', 4, 'marian@gmail.com'),
(128, 6, 'b14', 4, 'marian@gmail.com'),
(129, 6, 'b20', 12, 'vmalto@gmail.com'),
(130, 6, 'b107', 6, 'susanita@gmail.com'),
(131, 4, 'b01', 6, 'raul@gmail.com'),
(132, 4, 'b02', 6, 'raul@gmail.com'),
(133, 4, 'b03', 6, 'raul@gmail.com'),
(134, 4, 'b14', 6, 'raul@gmail.com'),
(152, 4, 'b18', 8, 'tomas@gmail.com'),
(153, 4, 'b17', 8, 'tomas@gmail.com'),
(154, 4, 'b20', 8, 'tomas@gmail.com'),
(155, 4, 'b19', 8, 'tomas@gmail.com'),
(156, 4, 'b06', 8, 'tomas@gmail.com'),
(157, 4, 'b08', 8, 'tomas@gmail.com'),
(158, 4, 'b07', 8, 'tomas@gmail.com'),
(159, 4, 'b05', 8, 'tomas@gmail.com'),
(160, 4, 'b29', 8, 'tomas@gmail.com'),
(161, 4, 'b32', 8, 'tomas@gmail.com'),
(200, 6, 'b107', 10, 'yuya@hotmail.com'),
(201, 6, 'b108', 10, 'yuya@hotmail.com'),
(202, 6, 'b120', 10, 'yuya@hotmail.com'),
(203, 6, 'b118', 12, 'valerie@gmail.com'),
(204, 6, 'b117', 15, 'jaime@gmail.com'),
(205, 6, 'b92', 15, 'teresa@gmail.com'),
(206, 6, 'b107', 15, 'julios@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de la taula `pelicula`
--

CREATE TABLE `pelicula` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `imagen` varchar(80) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `sinopsis` text CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `duracion` int(11) NOT NULL,
  `genero` varchar(80) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `edad_min` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcant dades de la taula `pelicula`
--

INSERT INTO `pelicula` (`id`, `nombre`, `imagen`, `sinopsis`, `duracion`, `genero`, `edad_min`) VALUES
(1, 'Rompe Ralph 2', 'Ralph_destrueix_internet.jpg', 'Ralph sale de los recreativos y ¡rompe Internet! ¿Qué podría salir mal? La secuela de ¡Rompe Ralph! (2012) tendrá lugar en el presente, seis años después de los acontecimientos de la primera película. En ella volveremos a ver a Ralph, el malo del videojuego que quiere ser un héroe, junto a otros personajes ya conocidos como Vanellope von Schweetz, la joven "error de programación" con un imparable espíritu ganador. ', 112, 'animación', 3),
(2, 'Avengers: Infinity War', 'Avengers_Infinity_War.jpg', 'Después de haber adquirido la Gema del Poder del planeta Xandar en manos de los Nova Corps, el malvado titán Thanos y sus lugartenientes, Ebony Maw, Cull Obsidian, Próxima Midnight y Corvus Glaive, interceptan la nave espacial que transportaba a los sobrevivientes de la destrucción de Asgard. A medida que extraen la Gema del Espacio del Teseracto, Thanos somete a Thor, domina a Hulk y mata a Loki. Mientras que Heimdall usa lo que le queda de energía oscura para enviar a Bruce Banner / Hulk de regreso a la Tierra invocando el Bifröst antes de ser asesinado por Thanos. Una vez que cumple su cometido, Thanos se va con sus lugartenientes y destruye la nave espacial.', 149, 'Superhéroes', 7),
(3, 'Bohemian Rhapsody', 'Bohemian_Rhapsody.png', 'Bohemian Rhapsody es una rotunda y sonora celebración de Queen, de su música y de su extraordinario cantante Freddie Mercury, que desafió estereotipos e hizo añicos tradiciones para convertirse en uno de los showmans más queridos del mundo. La película plasma el meteórico ascenso al olimpo de la música de la banda a través de sus icónicas canciones y su revolucionario sonido, su crisis cuando el estilo de vida de Mercury estuvo fuera de control, y su triunfal reunión en la víspera del Live Aid, en la que Mercury, mientras sufría una enfermedad que amenazaba su vida, lidera a la banda en uno de los conciertos de rock más grandes de la historia. Veremos cómo se cimentó el legado de una banda que siempre se pareció más a una familia, y que continúa inspirando a propios y extraños, soñadores y amantes de la música hasta nuestros días.', 132, 'Drama', 12),
(4, 'Spider-Man: Homecoming', 'spider_home.jpg', 'En la película veremos como Peter (Tom Holland) vuelve a casa emocionado después de su experiencia con los Vengadores en Capitán América: Civil War. Allí vive con su tía May (Marisa Tomei), bajo la atenta mirada de su nuevo mentor Tony Stark (Robert Downey Jr.). Peter trata de volver a su rutina diaria, aunque siempre distraído en sus pensamientos, intentando demostrar que es alguien más que el amigable vecino Spider-Man. Pero cuando alguien conocido como el Buitre emerge como villano, todo lo que Peter considera más importante en su vida se verá amenazado.', 133, 'Superhéroes', 7),
(5, 'Mortal Engines', 'Mortal_Engines.jpg', 'Hace miles de años, un cataclismo destruyó la civilización tal y como la conocemos hoy en día. La humanidad se ha tenido que adaptar a esta nueva era post-apocalíptica, desarrollando un nuevo estilo de vida. Ahora la amenaza son unas ciudades móviles gigantes que asedian sin piedad poblaciones menores. \r\n', 128, 'Ciencia Ficción', 7),
(9, 'Bumblebee', 'Bbee.png', '20 años antes de los acontecimientos de la primera película, Durante 1987,Bumblebee encuentra refugio en un depósito de chatarra en una pequeña ciudad en california. Charlie Watson (Hailee Steinfeld), a punto de cumplir 18 años y tratando de encontrar su lugar en el mundo, descubre a Bumblebee, con cicatrices de batalla y roto.', 230, 'Ciencia ficcion', 7);

-- --------------------------------------------------------

--
-- Estructura de la taula `sesiones`
--

CREATE TABLE `sesiones` (
  `id` int(10) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `id_pelicula` int(11) NOT NULL,
  `fila_vip` tinyint(1) NOT NULL DEFAULT '0',
  `dia_espectador` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcant dades de la taula `sesiones`
--

INSERT INTO `sesiones` (`id`, `fecha`, `hora`, `id_pelicula`, `fila_vip`, `dia_espectador`) VALUES
(1, '2018-12-05', '20:30:00', 1, 1, 1),
(2, '2018-12-20', '19:30:00', 5, 0, 0),
(3, '2018-12-14', '20:30:00', 3, 0, 0),
(4, '2018-12-30', '22:00:00', 2, 0, 0),
(5, '2018-12-02', '19:00:00', 4, 0, 0),
(6, '2018-12-26', '20:30:00', 3, 1, 1),
(7, '2018-12-06', '21:30:00', 2, 0, 0),
(8, '2018-12-19', '19:00:00', 4, 1, 1),
(9, '2018-12-16', '20:00:00', 4, 0, 0),
(10, '2018-12-29', '22:00:00', 4, 0, 0),
(11, '2018-12-01', '19:00:00', 3, 0, 0),
(12, '2018-12-27', '20:00:00', 4, 0, 0),
(13, '2018-12-08', '22:00:00', 4, 0, 0),
(15, '2018-12-28', '22:00:00', 9, 1, 0);

-- --------------------------------------------------------

--
-- Estructura de la taula `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) NOT NULL,
  `apellidos` varchar(80) NOT NULL,
  `telefono` int(9) NOT NULL,
  `correo` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Bolcant dades de la taula `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `apellidos`, `telefono`, `correo`) VALUES
(1, 'Carles', 'Vilà', 656378926, 'cvila@gmail.com'),
(3, 'Maria ', 'Belén', 655435326, 'mbelen@gmail.com'),
(4, 'Joan', 'Vila', 634534543, 'joan@gmail.com'),
(5, 'Victor', 'Apend', 565465464, 'victor@gmail.com'),
(6, 'mabe', 'chavez', 698345670, 'mabe@gmail.com'),
(7, 'Albaro', 'Segundo', 656478326, 'albaro@iam.cat'),
(8, 'carla', 'pujades', 698345611, 'carla@gmail.com'),
(9, 'Adria', 'Ortiz', 656464565, 'adria@iam.cat'),
(10, 'marta', 'pineda', 635265984, 'marta@gmail.com'),
(11, 'Francesc', 'Romeu', 645654565, 'francesc@iam.cat'),
(12, 'sergi', 'martos', 698654789, 'sergi@gmail.com'),
(13, 'Oriol', 'Palou', 973480993, 'oriol@iam.cat'),
(14, 'Gerard', 'Farras', 545345434, 'gerard@gmail.com'),
(15, 'Eduard', 'Torra', 654654565, 'eduard@gmail.com'),
(16, 'carlos', 'garcia', 698569856, 'carlos@gmail.com'),
(17, 'Marc', 'Sala', 654345675, 'marc@gmail.com'),
(18, 'Joan', 'Vila', 973480943, 'joan_vila@iam.cat'),
(19, 'victor', 'malto', 698658965, 'vmalto@gmail.com'),
(20, 'Juan', 'Jose', 654654565, 'juan_jose@gmail.com'),
(21, 'Marian', 'Jose', 654435434, 'marian@gmail.com'),
(22, 'susana', 'lamala', 698563221, 'susanita@gmail.com'),
(23, 'Valerie', 'Moncayo', 658963214, 'valerie@gmail.com'),
(24, 'lola', 'saray', 698562569, 'lola@gmail.com'),
(25, 'teresa', 'zambrano', 985698521, 'teresa@gmail.com'),
(26, 'Pere', 'Requena', 985654752, 'pere@gmail.com'),
(27, 'marcos', 'bertazza', 698563568, 'marcos@gmail.com'),
(28, 'raul', 'vazquez', 698569874, 'raul@gmail.com'),
(29, 'Tomas', 'Silva', 654765645, 'tomas@gmail.com'),
(30, 'yuya', 'machado', 698563254, 'yuya@hotmail.com'),
(31, 'jaime', 'perez', 995456789, 'jaime@gmail.com'),
(32, 'julio', 'suarez', 123456798, 'julios@gmail.com'),
(33, 'adfasf', 'afsf', 956856963, 'sdfs@gmail.com');

--
-- Indexos per taules bolcades
--

--
-- Index de la taula `entrada`
--
ALTER TABLE `entrada`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_sesion` (`id_sesion`),
  ADD KEY `correo` (`correo`);

--
-- Index de la taula `pelicula`
--
ALTER TABLE `pelicula`
  ADD PRIMARY KEY (`id`);

--
-- Index de la taula `sesiones`
--
ALTER TABLE `sesiones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_pelicula` (`id_pelicula`);

--
-- Index de la taula `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `correo` (`correo`);

--
-- AUTO_INCREMENT per les taules bolcades
--

--
-- AUTO_INCREMENT per la taula `entrada`
--
ALTER TABLE `entrada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;
--
-- AUTO_INCREMENT per la taula `pelicula`
--
ALTER TABLE `pelicula`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT per la taula `sesiones`
--
ALTER TABLE `sesiones`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT per la taula `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- Restriccions per taules bolcades
--

--
-- Restriccions per la taula `entrada`
--
ALTER TABLE `entrada`
  ADD CONSTRAINT `entrada_ibfk_1` FOREIGN KEY (`correo`) REFERENCES `usuario` (`correo`),
  ADD CONSTRAINT `entrada_ibfk_2` FOREIGN KEY (`id_sesion`) REFERENCES `sesiones` (`id`);

--
-- Restriccions per la taula `sesiones`
--
ALTER TABLE `sesiones`
  ADD CONSTRAINT `sesiones_ibfk_1` FOREIGN KEY (`id_pelicula`) REFERENCES `pelicula` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
